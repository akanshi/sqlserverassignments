create table Worker(worker_id int constraint pk primary key,
first_name varchar(25) not null,
last_name varchar(25),
salary int check(salary between 10000 and 25000),
joining_date datetime ,
department char(25) check(department in ('hr','sales','accts','it')));

insert into Worker(worker_id,first_name,last_name,salary,department)
values(1,'akanshi','gupta',20000,'hr');

insert into Worker(worker_id,first_name,last_name,salary,department)
values(2,'manas','gupta',20000,'hr');

select * from Worker;
--ques 1
select first_name as worker_name from Worker;

--ques2
select UPPER(first_name) as worker_name from Worker;

--ques3
select distinct department from Worker;

--ques 4
select SUBSTRING(first_name,1,3) from Worker;

--ques5
select CHARINDEX('K',first_name) from Worker where first_name = 'akanshi';

--ques 6
SELECT  RTRIM(first_name) FROM worker;

--ques 7
SELECT LTRIM(department) FROM worker;

--ques 8
SELECT DISTINCT department, LEN(department) AS departmentNameLength FROM worker;

--ques 9
SELECT REPLACE(first_name,'ak','ss') FROM Worker;

--ques 10
SELECT CONCAT(first_name, last_name) AS complete_name FROM worker;

--ques 11
SELECT * FROM Worker ORDER BY first_name;

--ques 12
SELECT * FROM Worker ORDER BY first_name ASC,department DESC;

--ques 13
SELECT * FROM Worker WHERE first_name IN ('akanshi','Satish');

--ques 14
SELECT * FROM Worker WHERE first_name NOT IN ('satish','vipul');

--ques 15
SELECT * FROM Worker WHERE department = 'Admin';

--ques 16
SELECT * FROM worker WHERE first_name LIKE '%a%';

--ques 17
SELECT * FROM Worker WHERE first_name LIKE '%a';

--ques 18
SELECT * FROM worker WHERE first_name LIKE '%h' and LEN(first_name) = 6;

--ques 19
SELECT * FROM Worker WHERE salary BETWEEN 100000 and 50000;

--ques 20
SELECT * FROM worker WHERE joining_date LIKE '2014-02%';

--ques 21
SELECT COUNT(*) FROM Worker WHERE department = 'Admin';
